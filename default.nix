/*
This is a nix expression to build python2mse from source on any distro
where nix is installed. This will install all the dependencies from
the nixpkgs repo and build python2mse without interfering with the host
distro.

http://nixos.org/nix/

To quickly install nix, you can run the following command:

$ curl -L http://git.io/nix-install.sh | bash

To initialize it:

$ source ~/.nix-profile/etc/profile.d/nix.sh

To build python2mse, from the current directory:

$ nix-build

To run the newly compiled python2mse:

$ ./result/bin/py2mse
*/

let
  pkgs = import <nixpkgs> {};
  pythonpkgs = pkgs.python2Packages;
in 
pythonpkgs.buildPythonPackage rec {
  name = "python2mse-git";

  src = ./.;

  propagatedBuildInputs = [ pythonpkgs.astroid pkgs.pylint ];

  # this does not work because of an import problem (but I don't
  # understand):
  checkPhase = ''PYTHONPATH=$PYTHONPATH:. python test/unittest_py2mse.py'';

  # simpleparse is only used by the unit tests
  buildInputs = [ pythonpkgs.simpleparse ];

}
