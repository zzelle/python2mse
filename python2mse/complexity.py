'''This module contains the ComplexityVisitor class which is where all the
analysis concerning Cyclomatic Complexity is done. There is also the class
HalsteadVisitor, that counts Halstead metrics.'''


from python2mse.visitor import Visitor
from recursivevisitor import RecursiveVisitor
from astroid.utils import ASTWalker

class ComplexityAnnotator(ASTWalker):
    def __init__(self):
        super(ComplexityAnnotator, self).__init__(ComplexityVisitor())

    def visit(self, node):
        defaultm = getattr(self.handler, 'visit_all', None)
        if defaultm is not None:
            defaultm(node)
        super(ComplexityAnnotator, self).visit(node)

    def leave(self, node):
        super(ComplexityAnnotator, self).leave(node)
        defaultm = getattr(self.handler, 'leave_all', None)
        if defaultm is not None:
            defaultm(node)

class ComplexityVisitor(Visitor):

    def __init__(self):
        self.function_heap = []
        self._node = None
        self._child_node = None
        super(ComplexityVisitor, self).__init__()

    def set_context(self, node, child_node):
        self._node = node
        self._child_node = child_node

    def visit_function(self, node):
        node.complexity = 1
        node.statements = 0
        self.function_heap.append(node)

    def leave_function(self, node):
        self.function_heap.pop()

        # add the computed complexity of the function to the parent
        # function, if any
        self._add_complexity(node.complexity)

    def _add_complexity(self, value):
        if self.function_heap:
            self.function_heap[-1].complexity += value

    def _add_statements(self, value=1):
        if self.function_heap:
            self.function_heap[-1].statements += value

    def visit_all(self, node):
        """default visit method -> increments the statements counter if
        necessary
        """
        if node.is_statement:
            self._add_statements()

    def visit_tryexcept(self, node):
        self._add_complexity(len(node.handlers) + len(node.orelse))
    def visit_boolop(self, node):
        self._add_complexity(len(node.values) - 1)
    def visit_lambda(self, node):
        self._add_complexity(1)
    def visit_if(self, node):
        self._add_complexity(1)
    def visit_ifexp(self, node):
        self._add_complexity(1)
    def visit_with(self, node):
        self._add_complexity(1)
    def visit_for(self, node):
        self._add_complexity(bool(node.orelse) + 1)
    def visit_while(self, node):
        self._add_complexity(bool(node.orelse) + 1)
    def visit_comprehension(self, node):
        self._add_complexity(len(node.ifs) + 1)


