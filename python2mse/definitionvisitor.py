import sys
import os.path as osp
import StringIO

from recursivevisitor import ParentRecursiveVisitor
import astroid
from astroid.as_string import AsStringVisitor
from astroid.node_classes import (AssAttr, AssName, Tuple, Subscript, Instance,
                                  Const, Arguments, Name)
from astroid.scoped_nodes import Class, Module, Function, CallFunc, Lambda, GenExpr
from astroid.bases import BoundMethod, UnboundMethod, InferenceError, NotFoundError, YES
import six

class DefinitionVisitor(ParentRecursiveVisitor):

    def __init__(self, out, refmgr):
        super(DefinitionVisitor, self).__init__()
        self.out = out
        self.ref_mgr = refmgr

    def close(self):
        "write MSE node for every unmanaged nodes as stub"
        missing = (set(self.ref_mgr.values()) - self.ref_mgr.managed)
        missing = [node for (node, ref) in self.ref_mgr.items() if ref in missing]
        for node in missing:
            func = getattr(self, "visit_" + node.__class__.__name__.lower(), None)
            if func:
                func(node)
            if not self.ref_mgr.is_managed(node):
                nodeid = self.ref_mgr.ensure_entity(node)
                self.write('(FAMIX.PyUnknown (id: %s)' % nodeid)
                self.write(')')
        # check no mseid has been given twice
        assert len(self.ref_mgr) == len(set(self.ref_mgr.values()))

    def write(self, bytes):
        self.out.write(bytes)
        self.out.write('\n')

    def leave_all(self, node):
        # pylint: disable=missing-docstring
        self._done.add(node)
        if not self.ref_mgr.is_stub(node):
            # do only visit children if not stub
            for child in node.get_children():
                if child not in self._done:
                    child.accept(self)

    # visitor methods
    def visit_module(self, node):
        self.currentModule = node
        self.importedNamed = node.name

        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        isstub = self.ref_mgr.is_stub(node)
        if not isstub:
            anchor = self.write_source_anchor(node)
        self.write("(FAMIX.PyModule (id: %s)" % nodeid)
        self.write("  (name '%s' )" % node.name)
        self.write('  (isPackage %s)' % str(node.package).lower())
        self.write('  (isStub %s)' % str(isstub).lower())
        if not isstub and node.parent is not None:
            self.write('  (parent (ref: %s))' %
                       self.ref_mgr.ensure_entity(node.parent, False))
        if not isstub:
            self.write_source_anchorRef(anchor)
        self.write(')')
        if not isstub and self._has_comment(node):
            self.write_comment(node, nodeid)

    def visit_class(self, node):
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        isstub = self.ref_mgr.is_stub(node)
        if not isstub:
            anchor = self.write_source_anchor(node)
        self.write('(FAMIX.PyClass (id: %s)' % nodeid)
        self.write("  (name '%s')" % node.name)
        self.write('  (isStub %s)' % str(isstub).lower())
        if not isstub:
            self.write('  (container (ref: %s))' %
                       self.ref_mgr.ensure_entity(node.parent, False))
            self.write_source_anchorRef(anchor)
        self.write(')')

        if not isstub:
            if (self._has_comment(node)):
                self.write_comment(node, nodeid)
            for nodename in node.bases:
                try:
                    base = list(nodename.infer())
                except InferenceError:
                    # baseclass is a stub
                    base = [Class(nodename.as_string(), '')]
                    base[0].accept(self)

                for b in base:
                    if b is YES:
                        continue
                    baseid = self.ref_mgr.ensure_entity(b, False)
                    self.write('(FAMIX.Inheritance (id: %s)' % self.ref_mgr.ensure_entity((nodeid, baseid)))
                    self.write('  (subclass (ref: %s))' % nodeid)
                    self.write('  (superclass (ref: %s))' % baseid)
                    self.write(')')
            super(DefinitionVisitor, self).visit_class(node)

    def visit_arguments(self, node):
        scope = node.parent
        if node.vararg:
            id = self.ref_mgr.ensure_entity((node, 'vararg'))
            self.write('(FAMIX.PyParameter (id: %s )' % str(id))
            self.write('  (name %r)' % node.vararg)
            self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        if node.kwarg:
            id = self.ref_mgr.ensure_entity((node, 'kwarg'))
            self.write('(FAMIX.PyParameter (id: %s )' % str(id))
            self.write('  (name %r)' % node.kwarg)
            self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')

    def visit_function(self, node):
        if self.ref_mgr.is_managed(node):
            return

        isstub = self.ref_mgr.is_stub(node)
        if isstub:
            self.write('(FAMIX.PyFunction (id: %s) ' % str(self.ref_mgr.ensure_entity(node)))
            self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(node.root(), False)))
            self.write("  (name '%s')" % node.name)
            self.write("  (signature '%s(%s)')" % (node.name, node.args.accept(AsStringVisitor()).replace("'", "''")))

            self.write('  (isStub true)')
            self.write(')')
            return

        nodeid = self.ref_mgr.ensure_entity(node)
        anchor = self.write_source_anchor(node)
        if (node.is_method()):
            self.write('(FAMIX.PyMethod (id: %s)' % str(nodeid))
            self.write('  (parentType (ref: %s))' % str(self.ref_mgr.ensure_entity(node.parent, False)))
        else:
            self.write('(FAMIX.PyFunction (id: %s) ' % str(nodeid))
            #self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(self.currentModule)))
            self.write('  (container (ref: %s))' % str(self.ref_mgr.ensure_entity(node.parent, False)))
        self.write("  (name '%s')" % node.name)
        self.write("  (signature '%s(%s)')" % (node.name, node.args.accept(AsStringVisitor()).replace("'", "''")))
        self.write_source_anchorRef(anchor)
        if node.args.args is not None:
            self.write("  (numberOfParameters %d)" % (len(node.args.args)))
        if hasattr(node, 'statements'):
            self.write("  (numberOfStatements %d)" % (node.statements))
        if hasattr(node, 'complexity'):
            self.write("  (cyclomaticComplexity %d)" % (node.complexity))

        self.write(')')
        if (not isstub and self._has_comment(node)):
            self.write_comment(node, nodeid)

    def visit_callfunc(self, node):
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        try:
            # when visiting a CallFunc, we try to guess what is the called
            # function. This is done in infering node.func
            # we first build a list of possible functions
            funcs = []
            # node.func may itself be a CallFunc, let's find the one
            # which is not a CallFunc, on which we might get a chance to infer
            func = node
            while isinstance(func, CallFunc):
                func = func.func
            for func in func.infer():
                if isinstance(func, (Function, BoundMethod)):
                    func = func.scope()
                if isinstance(func, Const):
                    for ifunc in func.infer():
                        if ifunc.callable():
                            funcs.append(ifunc)
                else:
                    funcs.append(func)

            # let's now build a list of possible candidates for
            # the called function, using the __call__ and __init__
            # special methods when needed
            candidates = []
            for func in funcs:
                if isinstance(func, Class):
                    try:
                        candidates.extend(func.getattr('__init__'))
                    except NotFoundError:
                        # XXX should do things here I guess
                        pass
                elif isinstance(func, Instance):
                    try:
                        candidates.extend(func.getattr('__call__'))
                    except NotFoundError:
                        # XXX shuold do things here I guess
                        pass
                elif func is YES:
                    # could not infer the called function, create a stub function
                    if hasattr(node.func, 'name'):
                        funcname = node.func.name
                    elif hasattr(node.func, 'attrname'):
                        funcname = node.func.attrname
                    else:
                        # cannot say anuything about the called function,
                        # think for example of something like
                        # z = funclist[idx](args)
                        funcname = 'N/A'

                    stubnode = 'stub<{0}:{1}>'.format(nodeid, funcname)
                    self.write('(FAMIX.PyFunction (id: %s)' % str(self.ref_mgr.ensure_entity(stubnode)))
                    self.write("  (name '%s')" % funcname)
                    self.write('  (isStub true)')
                    self.write(')')
                    candidates.append(stubnode)
                else:
                    candidates.append(func)
        except InferenceError:
            candidates = [] # XXX can probably be a bit smarter than this

        self.write('(FAMIX.Invocation (id: %s)' % str(nodeid))
        if candidates:
            funcrefs = ['(ref: %s)' % self.ref_mgr.ensure_entity(func, False)
                        for func in candidates]
            self.write('  (candidates %s)' % ' '.join(funcrefs))
        self.write("  (signature '%s')" % node.parent.value.as_string().replace("'", "''"))
        self.write('  (sender (ref: %s))' % self.ref_mgr.ensure_entity(node.scope(), False))
        try:
            receiver = node.func.infered()
            self.write('  (receiver {})'.format(' '.join('(ref: {})'.format(self.ref_mgr.ensure_entity(recv, False)) for recv in receiver)))
        except InferenceError:
            pass
        self.write(')')
        for func in candidates:
            if hasattr(func, 'callable') and func.callable() and func not in self._done:
                self.visit_function(func)


    def visit_assign(self, node):
        for target in node.targets:
            if self.ref_mgr.is_managed((target, node)):
                continue

    def visit_assattr(self, node):
        if self.ref_mgr.is_managed(node):
            return
        self.write('(FAMIX.Attribute (id: %s)' % self.ref_mgr.ensure_entity(node))
        self.write("  (name '%s')" % node.attrname)
        try:
            instance = node.expr.infer().next()
        except InferenceError:
            self.write("  (isStub true)")
        else:
            clsvar = not isinstance(instance, Instance)
            self.write("  (hasClassScope %s)" % str(clsvar).lower())
            self.write("  (isStub false)")
            self.write("  (parentType (ref: %s))" % self.ref_mgr.ensure_entity(instance.scope(), False))
        self.write(')')

    def visit_assname(self, node):
        if node.name == "self" and not isinstance(node.scope(), Function):
            return
        self._variable(node)

    def visit_getattr(self, node):
        try:
            for var in node.expr.infer():
                try:
                    attrs = var.getattr(node.attrname)
                    if attrs is YES:
                        continue
                    attr = attrs[0]  # XXX why only the first?
                    break
                except astroid.exceptions.NotFoundError:
                    continue
            else:
                # XXX don't know what to do if we can't infer this access
                return
        except InferenceError:
            return
        if isinstance(var, Module) and not attr in self.ref_mgr:
            self.write('(FAMIX.GlobalVariable (id: %s)' % self.ref_mgr.ensure_entity(attr))
            self.write("   (name '%s')" % node.attrname)
            self.write('   (parentScope (ref: %s))' % self.ref_mgr.ensure_entity(var, False))
            self.write(')')

    def visit_name(self, node):
        try:
            targets = node.infered()
            if targets:
                nodeid = self.ref_mgr.new_id()
                sourceid = self.ref_mgr.ensure_entity(node.scope(), False)
                self.write('(FAMIX.Reference (id: {})'.format(nodeid))
                self.write('  (source (ref: {}))'.format(sourceid))

                self.write('  (target {})'.format(' '.join('(ref: {})'.format(self.ref_mgr.ensure_entity(target, False)) for target in targets)))
                self.write(')')
        except InferenceError:
            pass

    def write_source_anchor(self, node):
        if node.fromlineno is None or node.tolineno is None:
            return
        sourceid = self.ref_mgr.new_id()
        self.write('(FAMIX.PyFileAnchor (id: %s)' % sourceid)
        self.write('  (startLine %d)' % (node.fromlineno))
        self.write('  (endLine %d)' % (node.tolineno))
        self.write('  (element (ref: %d))' % (self.ref_mgr[node]))
        def findfile(node):
            if node is None:
                return None
            if hasattr(node, 'file'):
                return node.file
            return findfile(node.parent)
        self.write("  (fileName '%s')" % findfile(node))
        self.write(')')
        return sourceid

    def write_comment(self, node, id):
        self.write('(FAMIX.Comment (id: %s)' % self.ref_mgr.new_id())
        self.write("  (content %r)" % node.doc)
        self.write("  (container (ref: %d))" % (id))
        self.write(')')

    def write_source_anchorRef(self, fileid):
        if fileid is not None:
            return self.write("  (sourceAnchor (ref: %s))" % (fileid))

    def _create_import(self, lname, name, node):
        try:
            mod = node.do_import_module(name)
        except InferenceError:
            # module unavailable
            stubnode = 'stub<PyModule:{0}>'.format(name)
            ref = self.ref_mgr.ensure_entity(stubnode)
            self.write('(FAMIX.PyModule (id: %s)' % ref)
            self.write("  (name '%s')" % name)
            self.write('  (isStub true)')
            self.write(')')
        else:
            mod.accept(self) # XXX
            ref = self.ref_mgr.get(mod)

        nodeid = self.ref_mgr.new_id()
        self.write('(FAMIX.PyModuleImport (id: %s)' % nodeid)
        self.write('   (name %r)' % lname)
        self.write('   (importedModule (ref: %s))' % ref)
        self.write('   (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(node.scope(), False))
        self.write(')')
        
    def visit_import(self, node):
        # an import is cnosidered as a variable assignment of a local
        # variable that references a Module object

        for name, localname in node.names:
            if '.' in name:
                names = name.split('.')
                if localname is None:
                    lname = names[-1]
                else:
                    lname = localname
                self._create_import(lname, names[-1], node)
            else:
                if localname is None:
                    if '.' in name:
                        lname = name.split('.')[0]

                    else:
                        lname = name
                else:
                    lname = localname
                
                self._create_import(lname, name, node)


    def visit_from(self, node):
        try:
            mod = node.do_import_module()
        except InferenceError: # will occur if the module is not available
            mod = None
        for name, localname in node.names:
            if name == '*':
                try:
                    assigned = next(mod.igetattr('__all__'))
                except (InferenceError, AttributeError):
                    # mod can be None (see above) or the __all__ may not be inferable
                    assigned = astroid.YES
                names = []
                if assigned is not astroid.YES:
                    for elt in getattr(assigned, 'elts', ()):
                        try:
                            elt_name = next(elt.infer())
                        except astroid.InferenceError:
                            continue

                        if not isinstance(elt_name, astroid.Const) \
                                 or not isinstance(elt_name.value, six.string_types):
                            continue
                        elt_name = elt_name.value
                        if elt_name in mod.locals:
                            names.append(elt_name)
                elif mod is not None:
                    names = [name for name in mod.locals.keys() if not name.startswith('_')]
                else:
                    names = []
            else:
                names = [name]
            for name in names:
                if mod is not None and name in mod.locals:
                    target = mod.locals[name][0]
                    target.accept(self) # XXX
                    ref = self.ref_mgr.get(target)
                else:
                    # probably some .so or dynamic stuff, let's say
                    # nothing about it but it exists somewhere...
                    ref = None
                if localname is None:
                    lname = name
                else:
                    lname = localname
                self._create_variable_for_import(lname, node.scope(), ref)

    def visit_subscript(self, node):
        if self.ref_mgr.is_managed(node):
            return


    # utility methods
    def _variable(self, node):
        decl = self.ref_mgr.get_declaration_entity(node)
        if decl is not None and decl is not node:
            # do not create a new Attribute or Variable if the node is
            # a new assignment for an already known variable
            return
        if self.ref_mgr.is_managed(node):
            return
        nodeid = self.ref_mgr.ensure_entity(node)
        if decl:
            scope = decl.scope()
        else:
            scope = node.scope()
        if isinstance(scope, Class):
            self.write('(FAMIX.Attribute (id: %s)' % nodeid)
            self.write("  (name '%s')" % node.name)
            self.write("  (hasClassScope true)")
            self.write("  (isStub false)")
            self.write("  (parentType (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        elif isinstance(scope, Module):
            self.write('(FAMIX.GlobalVariable (id: %s)' % nodeid)
            self.write("  (name '%s')" % node.name)
            self.write("  (isStub false)")
            self.write("  (parentScope (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            self.write(')')
        elif isinstance(scope, (Function, Lambda, GenExpr)):
            if isinstance(node.parent, Arguments):
                self.write('(FAMIX.PyParameter (id: %s )' % str(nodeid))
                self.write('  (name %r)' % node.name)
                self.write('  (parentBehaviouralEntity (ref: %s))' % self.ref_mgr.ensure_entity(scope, False))
                self.write(')')
            else:
                self.write('(FAMIX.LocalVariable (id: %s)' % nodeid)
                self.write("  (name '%s')" % node.name)
                self.write("  (isStub false)")
                self.write("  (parentBehaviouralEntity (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
                self.write(')')
        else:
            raise ValueError("Don't know how to convert {}".format(scope))
    def _create_variable_for_import(self, name, scope, ref=None, isstub=False):
        nodeid = self.ref_mgr.get_declaration_entity((name, scope))
        if nodeid is None: # the LocalVariable must be created
            nodeid = self.ref_mgr.ensure_entity((name, scope))
            if isinstance(scope, Module):
                self.write('(FAMIX.GlobalVariable (id: %s)' % nodeid)
                self.write("  (parentScope (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            else:
                self.write('(FAMIX.LocalVariable (id: %s)' % nodeid)
                if scope is not None:
                    self.write("  (parentBehaviouralEntity (ref: %s))" % self.ref_mgr.ensure_entity(scope, False))
            self.write("  (name '%s')" % name)
            self.write("  (isStub %s)" % str(bool(isstub)).lower())
            if ref:
                self.write("  (variable (ref: %s))" % ref)
            self.write(')')
        return nodeid

    def _has_comment(self, node):
        return node.doc not in (None, '')
