from astroid.nodes import ALL_NODE_CLASSES

print """
# Abstract visitor for node objects.
# Generated automatically by running generatevisitor.py

class Visitor(object):
    "A template Visitor for Astroid"

    def __call__(self, node):
        "Makes this visitor behave as a simple function"
        return node.accept(self)"""

for node in ALL_NODE_CLASSES:
    print """\
    def visit_%(node)s(self, node):
        "handle %(nodecls)s node"
        pass
""" % {'node': node.__name__.lower(), 'nodecls': node.__name__}
