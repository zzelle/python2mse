# -*- encoding: utf-8 -*-
#!/usr/bin/python
"""Main executable functions to convert a Python file/module to MSE and other tools
"""

import os
import sys
import re
from StringIO import StringIO
import pprint

from python2mse.definitionvisitor import DefinitionVisitor
from python2mse.complexity import ComplexityAnnotator
from python2mse.entitymanager import MSEReferenceManager
from python2mse.filesmanager import build_ASTs

def py2mse():
    "Convert python files passed as cmdline argument into MSE files"
    from optparse import OptionParser
    usage = "usage: %prog [options] file.py [file.py ...]"
    parser = OptionParser(usage)
    parser.add_option('--output', '-o',
                      help="Output file; if not set, write to stdout",
                      default=None)
    parser.add_option('--pylint', '-p', action="store_true",
                      help="Run pylint on the code", default=False)
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('Please provide directories with python files to convert')

    outfile = StringIO()

    entitymgr = MSEReferenceManager()
    for arg in args:
        entitymgr.add_root(arg)
    list_of_ast = build_ASTs(args)

    outfile.write("(")

    defvisitor = DefinitionVisitor(outfile, entitymgr)
    for filename, tree in list_of_ast:
        sys.stderr.write("*" * 80 + '\n')
        sys.stderr.write("CONVERTING {0}\n".format(filename))
        ComplexityAnnotator().walk(tree)
        tree.accept(defvisitor)
    defvisitor.close()

    outfile.write(")\n")

    # XXX
    out = outfile.getvalue()
    cwd = os.getcwd() + '/'
    out = out.replace(cwd, '')
    if opts.output is None:
        sys.stdout.write(out)
    else:
        outfile = open(opts.output, 'w').write(out)


def checkmsedata(data):
    "check there are no dandling reference in a MSE file"
    idreg = re.compile(r'\( *id *: *(?P<id>\d+) *\)', re.M)
    refreg = re.compile(r'\( *ref *: *(?P<id>\d+) *\)', re.M)
    ids = set(idreg.findall(data))
    refs = set(refreg.findall(data))

    return refs - ids


def checkmse():
    for fname in sys.argv[1:]:
        missing = checkmsedata(open(fname).read())
        if missing:
            print ("%s: missing IDs:\n  %s" % (fname,
                   ','.join(sorted(missing, key=lambda x:int(x)))))
        else:
            print "%s: no missing entity detected" % fname

def parsemse():
    # import here so we do not depend on simpleparse fo other tools
    from python2mse.mseparser import MSEParser
    if len(sys.argv) == 2:
        ifile = open(sys.argv[1])
    else:
        ifile = sys.stdin

    sucess, mses, n = MSEParser().parse(ifile.read())
    pprint.pprint(mses)

