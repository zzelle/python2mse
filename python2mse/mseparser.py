# pylint: disable=unused-argument,invalid-name,redefined-builtin,missing-docstring
"""A simpleparse-based MSE parser"""

from string import whitespace
from simpleparse.parser import Parser
from simpleparse.dispatchprocessor import DispatchProcessor

# pylint: disable=line-too-long
mse_grammar = r"""
### A simple mse parser
Root := Document
Document := space, OPEN, ElementNode*, CLOSE, space
ElementNode := OPEN, ELEMENTNAME, Serial?, AttributeNode*, CLOSE, space
Serial := OPEN, ID, INTEGER, CLOSE, space
AttributeNode := OPEN, SIMPLENAME, ValueNode*, CLOSE, space
ValueNode := (Primitive / Reference / ElementNode), space
Primitive := (STRING / NUMBER / Boolean), space
Boolean := (TRUE / FALSE), space
Reference := (IntegerReference / NameReference), space
IntegerReference := OPEN, REF, INTEGER, CLOSE, space
NameReference := OPEN, REF, ELEMENTNAME, CLOSE, space
OPEN := "(", space
CLOSE := ")", space
ID := "id:", space
REF := "ref:", space
TRUE := "true", space
FALSE := "false", space
ELEMENTNAME := letter, (letter / digit)*, (".", letter, ( letter / digit)* ), space
SIMPLENAME := letter, (letter / digit)*, space
INTEGER := digit+, space
NUMBER := "-"?, digit+, (".", digit+)?, (("e" / "E"),("-" / "+")?, digit+ )?, space
STRING := ("'", everythingButQuote*, "'")+, space
digit := [0-9]
letter := [a-zA-Z_]
comment := -'"'*
<quote_comment> := '"', comment, '"'
everythingButQuote := -[\\']
<space> := ([ \t\n\r] / quote_comment)*
"""

atom_end = set('()"\'') | set(whitespace)


class MSEReferenceError(Exception):
    pass


class MSEProcessor(DispatchProcessor):
    # pylint: disable=no-init, no-self-use
    """simpleparse Processor for parsing MSE files"""
    document = None

    def Document(self, (tag, left, right, children), buffer):
        self.document = []
        for child in children:
            self(child, buffer)
        # post treatment:resolve references (only IntegerReference is supported)
        edict = dict((node.eid, node) for node in self.document)
        for node in self.document:
            for k, values in node.items():
                for i, v in enumerate(values):
                    if isinstance(v, tuple) and v and v[0] == 'ref':
                        try:
                            values[i] = edict[v[1]]
                        except KeyError:
                            raise MSEReferenceError('Cannot find reference %s, '
                                                    'referenced from %s' %
                                                    (v[1], node))
                if len(values) == 1: # XXX is ti a good idea???
                    node[k] = values[0]
        return self.document

    def INTEGER(self, (tag, left, right, children), buffer):
        return int(buffer[left: right])

    def NUMBER(self, (tag, left, right, children), buffer):
        return float(buffer[left: right])

    def STRING(self, (tag, left, right, children), buffer):
        value = buffer[left: right].strip()[1: -1]
        return value

    def Boolean(self, (tag, left, right, children), buffer):
        if buffer[left: right] == "true":
            return True
        if buffer[left: right] == "false":
            return False

    def OPEN(self, (tag, left, right, children), buffer):
        pass

    def CLOSE(self, (tag, left, right, children), buffer):
        pass

    def ElementNode(self, (tag, left, right, children), buffer):
        name = self(children[1], buffer)
        attributes = dict([self(child, buffer) for child in children[2:-1]])
        if 'id' not in attributes:
            raise ValueError('ElementNode MUST have an ID')
        eid = attributes.pop('id')
        entity = MSEEntity(name, eid, **attributes) # pylint: disable=star-args
        self.document.append(entity)

    def ELEMENTNAME(self, (tag, left, right, children), buffer):
        return buffer[left: right].strip()

    def SIMPLENAME(self, (tag, left, right, children), buffer):
        return buffer[left: right].strip()

    def Serial(self, (tag, left, right, children), buffer):
        ID = self(children[2], buffer)
        return ('id', ID)

    def AttributeNode(self, (tag, left, right, children), buffer):
        attrname = self(children[1], buffer)
        values = [self(child, buffer) for child in children[2:-1]]
        return (attrname, values)

    def ValueNode(self, (tag, left, right, children), buffer):
        return self(children[0], buffer)

    def Primitive(self, (tag, left, right, children), buffer):
        prim = self(children[0], buffer)
        return prim

    def Reference(self, (tag, left, right, children), buffer):
        ref = self(children[0], buffer)
        return ref

    def IntegerReference(self, (tag, left, right, children), buffer):
        return ('ref', self(children[2], buffer))

    def NameReference(self, (tag, left, right, children), buffer):
        return ('ref', self(children[2], buffer))


class MSEParser(Parser):
    """simpleparse Parser for parsing MSE files"""

    def __init__(self, rule='Root'):
        Parser.__init__(self, mse_grammar, rule)

    def buildProcessor(self):
        return MSEProcessor()


class MSEEntity(dict):
    """Utility object (dict) that represents a entity in a MSE file"""
    def __init__(self, msetype, eid, **kwargs):
        self.msetype = msetype
        self.eid = eid
        super(MSEEntity, self).__init__(**kwargs)

    def __getattr__(self, key):
        return self[key]

    def __str__(self):
        out = ["%s (id:%s)"%(self.msetype, self.eid)]
        out.extend('   %s=%s' % (k, repr(v) if isinstance(v, MSEEntity) else v)
                   for k, v in sorted(self.items()))
        return '\n'.join(out)

    def __repr__(self):
        return "%s <id:%s>"%(self.msetype, self.eid)

    def __eq__(self, other):
        if self.msetype != other.msetype:
            return False
        if set(self.keys()) != set(other.keys()):
            return False
        for k, v in self.iteritems():
            if isinstance(v, MSEEntity):
                continue
            if other.get(k) != v:
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def filtered(self, keys):
        """return a copy of the entity with only keys listed in 'keys'"""
        kwargs = dict((k, v) for k, v in self.items() if k in keys)
        return self.__class__(self.msetype, self.eid, **kwargs)

    @property
    def fullname(self):
        base = ''
        if self.get('isStub') == 'true':
            base = '<stub>.'
        else:
            for ptype in 'container', 'parentType':
                if ptype in self:
                    base = self[ptype].fullname + '.'
                    break
        return base + self.name

    @property
    def name(self):
        if 'name' in self:
            return self['name']
        if self.msetype == 'FAMIX.PyFileAnchor':
            return '%s->%s@%s:%s' % (self.element.name, self.fileName,
                                     self.startLine, self.endLine)
        if self.msetype == 'FAMIX.Inheritance':
            return '%s<--%s' % (self.superclass.name, self.subclass.name)
        if self.msetype == 'FAMIX.Invocation':
            return '%s:call()' % (self.sender.name,)
        if self.msetype == 'FAMIX.Access':
            return '[%s] %s.%s' % (self.isWrite == 'true' and 'WRITE' or 'READ',
                                   self.accessor.name, self.variable.name)
        if self.msetype == 'FAMIX.Reference':
            return '%s-->%s' % (self.source.name, self.target.name)
        if self.msetype == 'FAMIX.PyUnknown':
            return 'Unknown'
        raise ValueError("Don't know how to build a name for %r", self)



if __name__ == '__main__':
    import sys
    from pprint import pprint
    pprint(MSEParser().parse(open(sys.argv[1]).read()))
