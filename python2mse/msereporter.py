"""
A pylint reporter that produces output usable in Moose

To use it, just run pylint with this reporter, like:

  # pylint -f python2mse.msereporter.MSEReporter myfile.py

(make sure python2mse is importable).
"""

from pylint.interfaces import IReporter
from pylint.reporters import BaseReporter
from pylint.utils import Message


from python2mse.entitymanager import MSEReferenceManager

TMPL = {'message': '''\
(Python.Pylint (id: {mseid})
   (messagedef (ref: {msgdef}))
   (description {msg})
   (sourceAnchor (ref: {srcref}))
)
''',
        'messagedef': '''\
(Python.PylintMessageDefinition (id: {mseid})
   (category {category})
   (name {symbol})
   (msgid {msgid})
)
''',
        'fileanchor': '''\
(FAMIX.PyFileAnchor (id: {mseid})
     (startLine {startline})
     (endLine {endline})
     (column {column})
     (element (ref: {refeid}))
     (fileName '{filename}')
)
''', }


MSEMGR = MSEReferenceManager()

class MSEReporter(BaseReporter):
    """A Pylint reporter that produces a MSE file"""
    # pylint: disable=abstract-method, missing-docstring
    __implements__ = IReporter
    name = 'mse'
    extension = 'mse'

    options = (
        ('generate-moose',
         {'default': False, 'type' : 'yn', 'metavar' : '<y_or_n>',
          'help': 'Generate the MOOSE metamodel'},
     ),
    )
    def __init__(self, output=None, msemanager=None):
        super(MSEReporter, self).__init__(output)
        self.msemgr = msemanager
        if self.msemgr is None:
            self.msemgr = MSEMGR
        self.messages = []

    def ensure_message_def(self, msg):
        msg_id = msg.msg_id
        msgdef = self.linter.msgs_store.check_message_id(msg_id)
        addmsgdef = msgdef not in self.msemgr
        msgdefref = self.msemgr.ensure_entity(msgdef)
        if addmsgdef:
            # pylint: disable=bad-continuation
            self.writeln(TMPL['messagedef'].format(
                    mseid=msgdefref,
                    category=self.msemgr.format(msg.category),
                    symbol=self.msemgr.format(msg.symbol),
                    msgid=self.msemgr.format(msg_id),))
        return msgdefref

    def handle_message(self, msg):
        msgdefref = self.ensure_message_def(msg)
        eid = self.msemgr.ensure_entity(msg)
        srcanchor = self.write_source_anchor(msg)
        self.messages.append(TMPL['message'].format(mseid=eid,
                                            msg=self.msemgr.format(msg.msg),
                                            msgdef=msgdefref,
                                            srcref=srcanchor,))

    def write_source_anchor(self, msg):
        if msg.line is None:
            return
        sourceid = self.msemgr.new_id()
        # pylint: disable=bad-continuation
        self.messages.append(TMPL['fileanchor'].format(
                mseid=sourceid,
                startline=msg.line,
                endline=msg.line,
                column=msg.column,
                refeid=self.msemgr[msg],
                filename=msg.path,))
        return sourceid

    def _display(self, layout):
        self.writeln('(\n')
        for msg in self.messages:
            self.writeln(msg)
        self.writeln(')\n')


from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker
from python2mse.complexity import ComplexityVisitor
from python2mse.definitionvisitor import DefinitionVisitor

class Py2MSEComplexityAnnotator(BaseChecker, ComplexityVisitor):
    """dummy checker which prepare the AST for MSE file generation generation
    """

    __implements__ = IAstroidChecker

    name = 'complexity'
    priority = -5
    force = True

    def __init__(self, linter=None):
        # needs to be implemented since BaseChecker.__init__ does not use super
        BaseChecker.__init__(self, linter)
        ComplexityVisitor.__init__(self)

class Py2MSE(BaseChecker, DefinitionVisitor):
    __implements__ = IAstroidChecker

    name = 'py2mse'
    priority = Py2MSEComplexityAnnotator.priority - 1 # must be executed after
    force = True

    def __init__(self, linter=None):
        # needs to be implemented since BaseChecker.__init__ does not use super
        BaseChecker.__init__(self, linter)
        import sys
        DefinitionVisitor.__init__(self, sys.stdout, MSEMGR)


def register(linter):
    """Register the reporter classes with the linter."""
    linter.register_reporter(MSEReporter)
    linter.register_checker(Py2MSEComplexityAnnotator(linter))
    linter.register_checker(Py2MSE(linter))
