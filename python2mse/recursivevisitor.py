"""Base visitor class for python to MSE converter
"""

from astroid.bases import NodeNG, InferenceError
from logilab.common.decorators import monkeypatch
from python2mse.visitor import Visitor

@monkeypatch(NodeNG)
def accept(self, visitor):
    # pylint: disable=missing-docstring
    if hasattr(visitor, '_done') and self in visitor._done:
        return
    if hasattr(visitor, 'visit_all'):
        visitor.visit_all(self)
    ret = None
    func = getattr(visitor, "visit_" + self.__class__.__name__.lower(), None)
    if func is None:
        func = getattr(visitor, "visit_default", None)
    if func:
        ret = func(self)
    if hasattr(visitor, 'leave_all'):
        visitor.leave_all(self)
    return ret

class Py2MSEWalker(object):
    def __init__(self, visitor):
        self._visitor = visitor

    def walk(self, node):
        cid = astroid.__class__.__name__.lower()
        # visit node
        node.accept(self._visitor)
        # recurse on children
        for child in node.get_children():
            self.walk(child)

class RecursiveVisitor(object):
    # pylint: disable=too-many-public-methods
    """A recursive visitor (visit the whole tree) that also ensure parent
    classes are visited"""
    def __call__(self, node):
        "Makes this visitor behave as a simple function"
        return node.accept(self)

    def __init__(self):
        # pylint: disable=missing-docstring
        self._done = set()
        super(RecursiveVisitor, self).__init__()

    def visit_all(self, node):
        # pylint: disable=missing-docstring
        self._done.add(node)
    def leave_all(self, node):
        # pylint: disable=missing-docstring
        for child in node.get_children():
            child.accept(self)

class ParentRecursiveVisitor(RecursiveVisitor):
    # pylint: disable=too-many-public-methods
    """A recursive visitor that also ensure parent
    classes are visited"""

    def visit_class(self, node):
        # pylint: disable=missing-docstring
        for nname in node.bases:
            try:
                for inferred in nname.infer():
                    inferred.accept(self)
            except InferenceError:
                pass # stub parent class

    def visit_import(self, node):
        # pylint: disable=missing-docstring
        for mname, mnode in node.names:
            if mnode is None:
                try:
                    node.do_import_module(mname)
                except InferenceError:
                    pass # XXX sure?
