# pylint: disable=missing-docstring
# Abstract visitor for node objects.
# Generated automatically by running generatevisitor.py

class Visitor(object):
    # pylint: disable=too-many-public-methods
    "A template Visitor for Astroid"

    def __call__(self, node):
        "Makes this visitor behave as a simple function"
        return node.accept(self)
    def visit_arguments(self, node):
        "handle Arguments node"
        pass

    def visit_assattr(self, node):
        "handle AssAttr node"
        pass

    def visit_assert(self, node):
        "handle Assert node"
        pass

    def visit_assign(self, node):
        "handle Assign node"
        pass

    def visit_assname(self, node):
        "handle AssName node"
        pass

    def visit_augassign(self, node):
        "handle AugAssign node"
        pass

    def visit_backquote(self, node):
        "handle Backquote node"
        pass

    def visit_binop(self, node):
        "handle BinOp node"
        pass

    def visit_boolop(self, node):
        "handle BoolOp node"
        pass

    def visit_break(self, node):
        "handle Break node"
        pass

    def visit_callfunc(self, node):
        "handle CallFunc node"
        pass

    def visit_class(self, node):
        "handle Class node"
        pass

    def visit_compare(self, node):
        "handle Compare node"
        pass

    def visit_comprehension(self, node):
        "handle Comprehension node"
        pass

    def visit_const(self, node):
        "handle Const node"
        pass

    def visit_continue(self, node):
        "handle Continue node"
        pass

    def visit_decorators(self, node):
        "handle Decorators node"
        pass

    def visit_delattr(self, node):
        "handle DelAttr node"
        pass

    def visit_delname(self, node):
        "handle DelName node"
        pass

    def visit_delete(self, node):
        "handle Delete node"
        pass

    def visit_dict(self, node):
        "handle Dict node"
        pass

    def visit_dictcomp(self, node):
        "handle DictComp node"
        pass

    def visit_discard(self, node):
        "handle Discard node"
        pass

    def visit_ellipsis(self, node):
        "handle Ellipsis node"
        pass

    def visit_emptynode(self, node):
        "handle EmptyNode node"
        pass

    def visit_excepthandler(self, node):
        "handle ExceptHandler node"
        pass

    def visit_exec(self, node):
        "handle Exec node"
        pass

    def visit_extslice(self, node):
        "handle ExtSlice node"
        pass

    def visit_for(self, node):
        "handle For node"
        pass

    def visit_from(self, node):
        "handle From node"
        pass

    def visit_function(self, node):
        "handle Function node"
        pass

    def visit_getattr(self, node):
        "handle Getattr node"
        pass

    def visit_genexpr(self, node):
        "handle GenExpr node"
        pass

    def visit_global(self, node):
        "handle Global node"
        pass

    def visit_if(self, node):
        "handle If node"
        pass

    def visit_ifexp(self, node):
        "handle IfExp node"
        pass

    def visit_import(self, node):
        "handle Import node"
        pass

    def visit_index(self, node):
        "handle Index node"
        pass

    def visit_keyword(self, node):
        "handle Keyword node"
        pass

    def visit_lambda(self, node):
        "handle Lambda node"
        pass

    def visit_list(self, node):
        "handle List node"
        pass

    def visit_listcomp(self, node):
        "handle ListComp node"
        pass

    def visit_name(self, node):
        "handle Name node"
        pass

    def visit_nonlocal(self, node):
        "handle Nonlocal node"
        pass

    def visit_module(self, node):
        "handle Module node"
        pass

    def visit_pass(self, node):
        "handle Pass node"
        pass

    def visit_print(self, node):
        "handle Print node"
        pass

    def visit_raise(self, node):
        "handle Raise node"
        pass

    def visit_return(self, node):
        "handle Return node"
        pass

    def visit_set(self, node):
        "handle Set node"
        pass

    def visit_setcomp(self, node):
        "handle SetComp node"
        pass

    def visit_slice(self, node):
        "handle Slice node"
        pass

    def visit_starred(self, node):
        "handle Starred node"
        pass

    def visit_subscript(self, node):
        "handle Subscript node"
        pass

    def visit_tryexcept(self, node):
        "handle TryExcept node"
        pass

    def visit_tryfinally(self, node):
        "handle TryFinally node"
        pass

    def visit_tuple(self, node):
        "handle Tuple node"
        pass

    def visit_unaryop(self, node):
        "handle UnaryOp node"
        pass

    def visit_while(self, node):
        "handle While node"
        pass

    def visit_with(self, node):
        "handle With node"
        pass

    def visit_yield(self, node):
        "handle Yield node"
        pass

    def visit_yieldfrom(self, node):
        "handle YieldFrom node"
        pass

