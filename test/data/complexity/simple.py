
# the complexity of each each function here will be compuited and
# compared with the value given as comment

def noop():
    '''
    complexity: 1
    statements: 1
    '''
    return

def simple_if(value):
    '''
    complexity: 2
    statements: 2
    '''
    if value:
        print value

def simple_if_return(value):
    '''
    complexity: 2
    statements: 3
    '''
    if value:
        return value
    return None

def simple_if_else(value):
    '''
    complexity: 2
    statements: 3
    '''
    if value:
        print value
    else:
        print "no value"

def simple_if_else_return(value):
    '''
    complexity: 2
    statements: 3
    '''
    if value:
        return value
    else:
        return "no value"

def simple_if_else_return2(value):
    '''
    complexity: 2
    statements: 4
    '''
    if value:
        return value
    else:
        print "no value"
    return None

def simple_for():
    '''
    complexity: 2
    statements: 2
    '''
    for i in xrange(10):
        print i
