
v1 = 1
v2 = 10
v3 = 0

def function(a, v2):
    global v3
    c = a + v1 + v2 # v1 is global
    v1 = v2 # v1 is local
    v3 = v1 + c # v1 is local, v3 is global

