def f(x):
    return bool(x)

def f1():
    a = -10
    while f(a):
        print "still looping"
        a = a + 1

#def f2():
#    a = -10
#    while not f(a):
#        print "still looping"
#        f = some_function_producer()

def g(v):
    return v < 10

def search(v):
    return v in range(3, 5)

def somecondition(v):
    return v % 2

def do_stuff(v):
    print ("Value is %s" % (v,))

def f3():
    i = 0
    while g(i):
        if search(i):
            print "found it"
            break
        i += 1
        if somecondition(i):
            continue
        do_stuff(i)
