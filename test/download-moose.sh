#! /usr/bin/env bash

wget -O boa.zip https://ci.inria.fr/rmod/job/Boa/OS=linux/lastSuccessfulBuild/artifact/boa_linux.zip

# extract the .image and .changes files into the current directory
unzip -j boa.zip boa/moose_suite_5_0/shared/moose_suite_5_0.image boa/moose_suite_5_0/shared/moose_suite_5_0.changes

mv moose_suite_5_0.image moose.image
mv moose_suite_5_0.changes moose.changes

rm -f boa.zip
