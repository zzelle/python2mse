#!/usr/bin/env bash

rm -rf pharo-vm
rm -f pharo pharo-ui
wget -O- get.pharo.org/vm40 | bash
wget http://files.pharo.org/sources/PharoV30.sources
mv PharoV30.sources pharo-vm
wget http://files.pharo.org/sources/PharoV40.sources
mv PharoV40.sources pharo-vm
