#! /usr/bin/env bash

PHARO_VM=${PHARO_VM:-./pharo}
PHARO_IMAGE=${BOA_IMAGE:-./moose.image}

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <file.mse>"
    exit 1
fi

mse=$(realpath $1)

if [[ ! -f $mse ]]; then
    echo "Can't find \"$mse\""
    exit 1
fi

cat > script.st <<EOF
| file model |
file := '${mse}' asFileReference.
model := BoaMooseModel new.
model name: file basenameWithoutExtension.

file readStreamDo: [ :stream |
  model importFromMSEStream: stream
].

Smalltalk snapshot: false andQuit: true
EOF

$PHARO_VM "$PHARO_IMAGE" --no-default-preferences $(pwd)/script.st
