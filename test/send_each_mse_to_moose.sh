#! /usr/bin/env bash

test_dir=data/py2mse/
moose=./moose.sh

if [[ $# -ne 0 ]]; then
    echo "Usage: $0"
    exit 1
fi

if [[ ! -d $test_dir ]]; then
    echo "Missing $test_dir"
    exit 1
fi

if [[ ! -x $moose ]]; then
    echo "$moose missing"
    exit 1
fi

MSE_FILES=$(mktemp --tmpdir tmp.mooseXXXXX)

# Use git when possible to list all dot files to install
$(find "$test_dir" -name '*.mse' -printf "%P\0" > "$MSE_FILES")

total=0
errors=0

while IFS='' read -r -d '' file <&9; do
    total=$(($total + 1))
    LOG=$(mktemp --tmpdir tmp.moose.logXXXXX)
    $moose "$test_dir/$file" > $LOG 2>&1
    if [[ $? -ne 0 ]]; then
        errors=$(($errors + 1))
        echo -n "[FAIL $file] "
        head -n 2 $LOG | tail -n 1
    fi
done 9< "$MSE_FILES"

echo
echo "Done: $errors erroneous mse files (total: $total)"
