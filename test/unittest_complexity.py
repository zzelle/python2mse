import os, os.path as osp
from astroid import builder, nodes

from python2mse.complexity import ComplexityAnnotator

from logilab.common.testlib import TestCase, InnerTest, unittest_main

DATA = osp.join(osp.dirname(__file__), 'data', 'complexity')

class ComplexityTC(TestCase):
    def check_complexity(self, node, expected):
        ComplexityAnnotator().walk(node)
        for k, v in expected.items():
            self.assertIn(k, dir(node))
            self.assertEqual(v, getattr(node, k), '%s: %s != %s'%(k, v, getattr(node, k)))

    def test_complexity(self):
        for pyfile in os.listdir(DATA):
            if pyfile.endswith('.py'):
                testname = pyfile[:-3]
                ast = builder.AstroidBuilder().file_build(osp.join(DATA, pyfile))
                for node in ast.body:
                    if isinstance(node, nodes.Function):
                        expected = [x.split(':') for x in node.doc.strip().splitlines()]
                        expected = dict((k.strip(), int(v)) for k,v in expected)

                        yield InnerTest('%s.%s'%(testname, node.name),
                                        self.check_complexity, node, expected)



if __name__ == '__main__':
    unittest_main()
