import os, os.path as osp
import sys
from utils import AbstractMSETC, msemap
from astroid import test_utils
from astroid import builder, MANAGER

from logilab.common.testlib import InnerTest

from python2mse import mseparser
from python2mse.complexity import ComplexityAnnotator
from python2mse.entitymanager import MSEReferenceManager

DATA = osp.join(osp.dirname(__file__), 'data', 'py2mse')

class MSEExamplesTC(AbstractMSETC):
    def check_example(self, msefile, skip_inexcess=True):
        if "testCase1" in msefile:
            self.skipTest("Not ready yet")
        modname = osp.basename(msefile)[:-4]
        dirname = osp.dirname(msefile)

        entitymgr = MSEReferenceManager()
        entitymgr.add_root(os.path.abspath(os.path.normpath(dirname)))

        # build the MSE file from Python module/package
        ast = MANAGER.ast_from_module_name(modname)
        # annotate the ast with complexity computations
        ComplexityAnnotator().walk(ast)
        mse = self.gen_mse(ast, entitymgr).strip()
        ref = open(msefile).read().strip()

        # parse the MSE files
        success, [refs], n = mseparser.MSEParser().parse(ref)
        self.assertTrue(success)
        success, [mses], n = mseparser.MSEParser().parse(mse)
        self.assertTrue(success)

        # keep only basenames for PyFileAnchor nodes
        for node in mses:
            if node.msetype == 'FAMIX.PyFileAnchor':
                if node.fileName.startswith(dirname):
                    node['fileName'] = node.fileName[len(dirname)+1:]
        mses = dict((x.fullname, x) for x in mses)
        refs = dict((x.fullname, x) for x in refs)

        missing = []
        inexcess = []
        notequal = []
        for epath, e in refs.items():
            if epath not in mses:
                missing.append(e)
            else:
                e2 = mses[epath]

                if skip_inexcess:
                    e2 = e2.filtered(e.keys())
                if e != e2:
                    notequal.append((e, e2))
        if not skip_inexcess:
            for epath, e in mses.items():
                if epath not in refs:
                    inexcess.append(e)

        if missing or inexcess or notequal:
            msg = ['\n']
            if missing:
                msg.append("missing:\n" + "\n".join(["  %s (%s)"%(e.fullname, e.eid) for e in missing]))
            if inexcess:
                msg.append("in excess:\n" + "\n".join(["  %s (%s)"%(e.fullname, e.eid) for e in inexcess]))
            if notequal:
                diff = ["  %s \n  <<<<<<\n  %s\n  ======\n  %s\n  >>>>>>" % (eref.fullname, str(eref), str(e)) for eref, e in notequal]
                msg.append("not equal:\n" + "\n".join(diff))
            self.assertTrue(False, '\n'.join(msg))

    def _test_file(self, msefile):
        msefile = osp.abspath(msefile)
        dirpath = osp.dirname(msefile)
        testbasename = '.'.join(dirpath[len(DATA)+1:].split('/'))
        pyfile = msefile[:-4] + '.py'

        testname = msefile[:-4]
        testname = '%s.%s' % (testbasename, testname)
        syspath = sys.path[:]
        sys.path.append(dirpath)
        try:
            self.check_example(msefile)
        finally:
            sys.path[:] = syspath

def add_test(dirpath, msefile):
    # DATA = test/data/py2mse
    # e.g., dirpath = "test/data/py2mse/foo/bar"
    # e.g., msefile = "test.mse"
    if msefile.endswith('.mse'):
        testbasename = '_'.join(dirpath[len(DATA)+1:].split('/'))
        # e.g., testbasename = "_foo_bar"

        testname = '%s_%s' % (testbasename, msefile[:-4])
        msefile = osp.join(dirpath, msefile)
        def test_mse(self, msefile=msefile):
            self._test_file(msefile)

        # make sure the test name is correct so pytest can filter
        # it out correctly
        test_mse.__name__ = 'test%s' % testname
        setattr(MSEExamplesTC, 'test%s' % testname, test_mse)


for dirpath, dirnames, filenames in os.walk(DATA):
    for msefile in filenames:
        add_test(dirpath, msefile)

if __name__ == "__main__":
    from unittest import main
    main()
