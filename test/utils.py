from StringIO import StringIO
from logilab.common.testlib import TestCase

from python2mse.definitionvisitor import DefinitionVisitor
from python2mse.entitymanager import MSEReferenceManager


class AbstractMSETC(TestCase):
    def gen_mse(self, ast, entitymgr=None):
        outfile = StringIO()
        outfile.write('(')
        if entitymgr is None:
            entitymgr = MSEReferenceManager()
        defvisitor = DefinitionVisitor(outfile, entitymgr)
        ast.accept(defvisitor)
        defvisitor.close()
        outfile.write(')\n')
        return outfile.getvalue()


def msemap(mse1, mse2):
    msemap = {}
    for e in mse1.values():
        for e2 in mse2.values():
            if e == e2:
                msemap[e.eid] = e2.eid
                break
        else:
            raise ValueError("cannot map %s" % e)
    return msemap
